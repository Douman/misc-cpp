# misc-cpp

[![Build](https://gitlab.com/Douman/misc-cpp/badges/master/build.svg)](https://gitlab.com/Douman/misc-cpp/pipelines)

Common pieces of C++ code I would always use.
