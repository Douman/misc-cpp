#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "misc.hpp"
#include "time.hpp"

void unreachable() {
    UNREACHABLE();
}

TEST_CASE("try") {
    REQUIRE(true);

    if (LIKELY(1 == 1)) {
        REQUIRE(true);
    } else {
        REQUIRE(false);
    }

    if (UNLIKELY(1 == 2)) {
        REQUIRE(false);
    } else {
        REQUIRE(true);
    }
}

int to_measure() {
    return 666;
}

TEST_CASE("time::measure") {
    int result = misc::time::measure(to_measure);
    REQUIRE(result == 666);
}
