#pragma once

#ifdef _MSC_VER

///Hints compiler that code here is unreachable
#define UNREACHABLE() __assume(0)

///Tells compiler to always inline
#define ALWAYS_INLINE __forceinline
///Tells compiler to not inline
#define NO_INLINE __declspec(noinline)

///Hint that condition is unlikely.
#define UNLIKELY(x) x
///Hint that condition is likely.
#define LIKELY(x) x

#elif defined(__clang__) || defined(__GNUC__)

#define UNREACHABLE() __builtin_unreachable()

#define ALWAYS_INLINE __attribute__((always_inline))
#define NO_INLINE __attribute__((noinline))

#define UNLIKELY(x) __builtin_expect((x), 0)
#define LIKELY(x) __builtin_expect(!!(x), 1)

#else

#define UNREACHABLE() abort()

#define ALWAYS_INLINE
#define NO_INLINE

#define UNLIKELY(x) x
#define LIKELY(x) x

#endif
